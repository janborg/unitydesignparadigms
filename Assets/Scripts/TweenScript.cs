﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class TweenScript : MonoBehaviour {

	Vector3 endPosition = new Vector3(6,0,0);




	void Start()
	{
		// DOTWEEN - note that it is in Start, since it is not supposed to start this every frame
//		transform.DOMove (endPosition, 2f, false);
	}

	void Update () {

		if (Input.GetKey (KeyCode.Space)) {
			UnityEngine.SceneManagement.SceneManager.LoadScene ("scene1");
		}


// 		MANUAL SCALE CHANGE 
//		float scaleFactor = 6;
//		float scaleX = transform.localScale.x;
//		float scaleY = transform.localScale.y;
//		float scaleZ = transform.localScale.z;
//
//		Vector3 newScale = new Vector3(scaleX += (scaleFactor -scaleX) * 0.1f, scaleY += (scaleFactor - scaleY) * 0.1f, scaleZ += (scaleFactor - scaleZ) * 0.1f);
//		transform.localScale = newScale;


// 		MANUAL X POSITION CHANGE
		float newXpos = (endPosition.x - transform.position.x) * 0.1f;   //	x += (target -x) * 0.1;
		transform.position = new Vector3(transform.position.x + newXpos,transform.position.y, transform.position.z); 


//		 LERP
//		float speed = 0.05f;
//		float step = speed * Time.deltaTime;
//		transform.position = Vector3.Lerp(transform.position, endPosition, speed);


//		 MOVETOWARDS
//		float speed = 6f;
//		float step = speed * Time.deltaTime;
//		transform.position = Vector3.MoveTowards(transform.position, endPosition, step);


//		SMOOTHDAMP
//		GameObject targetGameObject = GameObject.Find("targetSphere");
//		Vector3 targetPosition = targetGameObject.transform.position;
//		 Vector3 velocity = Vector3.zero;
//		transform.position = Vector3.SmoothDamp(transform.position, targetPosition, ref velocity, 0.3f);
		
	
	}
}
