﻿using UnityEngine;
using System.Collections;

public class Driving : MonoBehaviour {
	bool moving = true;
	Rigidbody myRigidBody;


    void OnEnable()
    {
        GameManager.OnTogglePause += Pause;
    }
    void OnDisable()
    {
        GameManager.OnTogglePause -= Pause;
    }

    void Pause()
    {
        print("shooting disabled in Shooting script");
        if (moving)
        {
            myRigidBody.Sleep();
        }
        moving = !moving;
    }

    // Use this for initialization
    void Start () {
		myRigidBody = gameObject.AddComponent<Rigidbody> ();
		myRigidBody.useGravity = false;
		StartCoroutine (MoveSequence ());
	}
	

	IEnumerator MoveSequence()
	{

		while (true) 
		{
            if (moving)
            { 
                myRigidBody.AddForce (20, 0, 0);
			    yield return new WaitForSeconds (1.5f);
            }
            if (moving)
            {
                myRigidBody.AddForce(0, 0, 20);
                yield return new WaitForSeconds(1.5f);
            }
            if (moving)
            {
                myRigidBody.AddForce(-40, 0, -40);
                yield return new WaitForSeconds(3);
            }
            yield return new WaitForSeconds(GameManager.Instance.pauseResumeTime);
        }

	
	}

}
