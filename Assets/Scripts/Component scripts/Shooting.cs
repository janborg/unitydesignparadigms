﻿using UnityEngine;
using System.Collections;

public class Shooting : MonoBehaviour {
	bool isShooting = true;
	public GameObject bullet;
	// Use this for initialization

	void OnEnable()
	{
		GameManager.OnTogglePause += ToggleShooting;
	}
	void OnDisable()
	{
        GameManager.OnTogglePause -= ToggleShooting;
	}

	void ToggleShooting()
	{
		print ("shooting disabled in Shooting script");
		isShooting = !isShooting;
	}

	void Start () {
		StartCoroutine (ShootingSequence ());
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	IEnumerator ShootingSequence()
	{
		while (true) {

            if (isShooting)
            { 
			    GameObject myBullet = (GameObject)Instantiate (bullet, transform.position, transform.rotation);
                yield return new WaitForSeconds(1);
            }
            yield return new WaitForSeconds(GameManager.Instance.pauseResumeTime);
        }

	}
}
