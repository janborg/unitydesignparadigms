﻿using UnityEngine;
using System.Collections;

public class EventManager : MonoBehaviour {

	public delegate void ShootingAction ();
	public static event ShootingAction OnToggleShooting;

	// Use this for initialization
	void Start () {
	
	}


    void OnGUI()
    {
        if (GUI.Button(new Rect(Screen.width / 2 - 50, 5, 100, 30), "Click"))
        {
            if (OnToggleShooting != null)
            {
                print("shooting toggle event fired");
                OnToggleShooting();
            }
        }
    }


}
