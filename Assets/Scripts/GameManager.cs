﻿using UnityEngine;
using System.Collections;

public class GameManager : Singleton<GameManager> {



	protected GameManager () {} // guarantee this will be always a singleton only - can't use the constructor!

	public string myGlobalVar = "whatever";
    public int score = 0;
	public float pauseResumeTime = 0.5f;
    // public delegate void PauseAction();
   // public static event PauseAction OnTogglePause;
    public static event System.Action OnTogglePause;

    void OnGUI()
    {
        if (GUI.Button(new Rect(Screen.width / 2 - 50, 5, 100, 30), "Pause"))
        {
            if (OnTogglePause != null)
            {
                print("shooting toggle event fired");
                OnTogglePause();
            }
        }
    }

}



